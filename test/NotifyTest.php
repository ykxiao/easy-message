<?php

declare(strict_types=1);

namespace EasyMessageTest;

use EasyMessage\Channel\DingTalkChannel;
use EasyMessage\Channel\FeiShuChannel;
use EasyMessage\Channel\WechatChannel;
use EasyMessage\Contracts\EasyMessageInterface;
use EasyMessage\Notify;
use EasyMessage\Template\Markdown;
use EasyMessage\Template\Text;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class NotifyTest extends TestCase
{
    public function testCase()
    {
        $dingTalkChannel = new DingTalkChannel();
        $feiShuChannel = new FeiShuChannel();
        $wechatChannel = new WechatChannel();

        $markdown = new Markdown();
        $text = new Text();

        $notify = Notify::make()->setChannel(DingTalkChannel::class)
            ->setAt(['all'])
            ->setTitle('标题')
            ->setText('测试')
            ->setPipeline(EasyMessageInterface::INFO)
            ->setTemplate(Markdown::class)
            ->send();

        $this->assertEquals($notify, true);
    }
}
