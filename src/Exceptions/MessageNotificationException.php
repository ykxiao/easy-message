<?php

declare(strict_types=1);

namespace EasyMessage\Exceptions;

class MessageNotificationException extends \RuntimeException
{
}
